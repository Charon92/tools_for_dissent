let button = document.getElementById('generate');
let text = document.getElementById('text');
let name = document.getElementById('name');
let encrypted_text_container = document.getElementById('encrypted_text');
let encryption_key = document.getElementById('encryptionKey');
let encrypt_section = document.getElementById('encryptSection');

let decrypt = document.getElementById('decrypt');
let text_to_decrypt = document.getElementById('encryptedText');
let key_file = document.getElementById('key');
let decrypted = document.getElementById('decrypted_text');
let decrypt_section = document.getElementById('decryptSection')

let encrypted_text = document.createElement('div');
let encrypted_text_item = document.createElement('p')
let file_download = document.createElement('a');
encrypted_text.classList.add('text--box');
file_download.classList.add('button', 'button--download');

let decrypted_text = document.createElement('div');
let decrypted_text_item = document.createElement('p')
decrypted_text.classList.add('text--box');

function getCookie(name) {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(';').shift();
}

button.addEventListener('click', function() {
    let text_value = text.value;
    let name_value = name.value;
    console.log(`Text ${text_value}. Name ${name_value}`);

    if (text_value === ''){
        text.classList.add('is-invalid');
        text.setAttribute('placeholder', 'We need some text to encrypt!')
    } else {

        let formData = new FormData();
        let fileField = encrypt_section.querySelector('input[type="file"]');

        formData.append('text', text_value);
        formData.append('name', name_value)
        formData.append('key_file', fileField.files[0])

        fetch("/api/v1/encrypt_text/", {
            method: "post",
            credentials: "same-origin",
            headers: {
                "X-CSRFToken": getCookie("csrftoken")
            },
            body: formData
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            encrypted_text_item.innerText = data.encrypted_text;
            encrypted_text.append(encrypted_text_item);
            encrypted_text_container.append(encrypted_text);
            if (data.key_file) {
                console.log(data.key_file);
                key_url = `${location.protocol}//${location.hostname}${data.key_file}`
                console.log(key_url);
                file_download.setAttribute('href', key_url);
                file_download.innerText = 'Download Key File';
                encrypted_text_container.append(file_download);
            }
        }).catch(function (ex) {
            console.log("parsing failed", ex);
        });
    }
})

decrypt.addEventListener('click', function () {
    let formData = new FormData();
    let fileField = decrypt_section.querySelector('input[type="file"]');
    let text = text_to_decrypt.value;

    formData.append('text', text);
    formData.append('key_file', fileField.files[0])

    fetch("/api/v1/decrypt_text/", {
        method: "post",
        credentials: "same-origin",
        headers: {
            "X-CSRFToken": getCookie("csrftoken")
        },
        body: formData
    }).then(function(response) {
        return response.json();
    }).then(function(data) {
        decrypted_text_item.innerText = data.decrypted;
        decrypted_text.append(decrypted_text_item);
        decrypted.append(decrypted_text);
    }).catch(function(ex) {
        console.log("parsing failed", ex);
    });
})