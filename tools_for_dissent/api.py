import base64
import os
import json

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend

from django.http.response import JsonResponse
from django.conf import settings
from django.core import serializers
from django.core.files.storage import FileSystemStorage

from tools_for_dissent.models import Tool


def get_tools():
    tools = serializers.serialize("json", Tool.objects.all())
    return JsonResponse(data=tools)


def generate_encryption_key(name):
    salt = os.urandom(16)
    key = Fernet.generate_key()
    # kdf = PBKDF2HMAC(
    #     algorithm=hashes.SHA256(),
    #     length=32,
    #     salt=salt,
    #     iterations=100000,
    #     backend=default_backend()
    # )
    # key = kdf.derive(key)

    file_name = f'{settings.MEDIA_ROOT}/keys/{name}.key'

    with open(f'{file_name}', 'wb') as f:
        f.write(key)

    return file_name


def load_key(key):
    """
    Loads the key from the current directory named `key.key`
    """
    import time

    i = 0
    while i < 5:
        try:
            file = open(key, "rb").read()
            break
        except FileNotFoundError:
            time.sleep(5)
            i += 1
    else:
        raise FileNotFoundError
    return file


def encrypt_text(request, *args, **kwargs):
    body = request.POST
    file = request.FILES if request.FILES else None

    print(f'----------\n BODY: {body}. FILE: {file} \n ----------')

    text = body['text'].encode()
    if file:
        key_file = file['key_file']
        fs = FileSystemStorage()
        filename = fs.save(key_file.name, key_file)
        uploaded_file_url = fs.url(filename)

        print(f'----------\n FILE LOCATION: {uploaded_file_url}\n ----------')

        key_file = f'{settings.BASE_DIR}{uploaded_file_url}'

        key = load_key(key_file)
    else:
        name = body['name']
        key_file = generate_encryption_key(name)

    key = load_key(key_file)
    f = Fernet(key)

    encrypted = f.encrypt(text)

    key_file_name = key_file.split('/')[-3:]
    key_file = '/'.join(key_file_name)
    print(key_file)

    data = {
        'encrypted_text': encrypted.decode(),
    }

    if not file:
        data['key_file'] = f'/{key_file}'

    return JsonResponse(data=data)


def decrypt_text(request, *args, **kwargs):
    body = request.POST
    file = request.FILES

    print(f'----------\n BODY: {body}. FILE: {file} \n ----------')

    text = body['text'].encode()
    key_file = file['key_file']

    fs = FileSystemStorage()
    filename = fs.save(key_file.name, key_file)
    uploaded_file_url = fs.url(filename)

    print(f'----------\n FILE LOCATION: {uploaded_file_url}\n ----------')

    key_file = f'{settings.BASE_DIR}{uploaded_file_url}'

    key = load_key(key_file)
    f = Fernet(key)

    unencrypted = f.decrypt(text)

    return JsonResponse(data={'decrypted': unencrypted.decode()})


def delete_file(request, *args, **kwargs):
    body = json.loads(request.body)
    name = body['name']

    if len(name.split('/')) > 1:
        name = name.split('/')[-1]

    os.remove(f'{settings.MEDIA_ROOT}/{name}')
