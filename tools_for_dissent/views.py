import base64
import os
import json

from typing import Union
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend

from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template.response import TemplateResponse
from django.contrib import messages
from django.views.generic import View, ListView
from django.template.defaultfilters import slugify
from django.urls import reverse
from honeypot.decorators import check_honeypot

from tools_for_dissent.models import Tool, Recommendation, Petition, Fund, BlogPage
from tools_for_dissent.forms import RecommendationForm


@check_honeypot(field_name='extra_hidden_field')
def index(request, *args, **kwargs) -> Union[TemplateResponse, HttpResponseRedirect]:
    import random

    tools = Tool.objects.all()[:4]
    funds = Fund.objects.all()[:4]
    petitions = Petition.objects.all()[:4]

    data = {
        'hero': {
            'text': random.choice(generate_random_quote())
        },
        'tools': tools,
        'funds': funds,
        'petitions': petitions
    }

    if request.method == 'POST':
        form = RecommendationForm(request.POST)

        if form.is_valid():
            name, website, description = form.cleaned_data['name'], form.cleaned_data['website'], form.cleaned_data['description']
            recommendation, created = Recommendation.objects.get_or_create(name=name, website=website, description=description)

            if created:
                data['created'] = True
                messages.success(request, 'Submitted! Thank you for your input', extra_tags='alert')
            else:
                data['created'] = False
                messages.warning(request, 'Suggestion has already been submitted but thank you for confirming this tools awesomeness!', extra_tags='alert')

            form = RecommendationForm()
        else:
            messages.warning(request, 'Please correct the errors below.', extra_tags='alert')
            data['form'] = form
            return TemplateResponse(request, 'index.html', context={'data': data})
    else:
        form = RecommendationForm()

    data['form'] = form
    return TemplateResponse(request, 'index.html', context={'data': data})


class PasswordGenerator(View):

    def generate(self, length=3, numbers=False):
        import random
        lines = open(f'{settings.BASE_DIR}/text/words.txt').read().splitlines()
        words = '-'.join(random.choices(lines, k=length))
        password = words[:-2]

        if numbers:
            password = password + f'-{random.randint(0,1000)}'

        return password

    def get(self, request, *args, **kwargs):
        password = self.generate()
        return TemplateResponse(request, 'password_generator.html', context={'generated_password': password})

    def post(self, request, *args, **kwargs):
        import json

        body = json.loads(request.body)

        length = int(body['length'])
        numbers = bool(body['numbers'])
        password = self.generate(length, numbers)

        return JsonResponse(data={'password': password})


password_generator = PasswordGenerator.as_view()


class AllTools(View):
    def get(self, request, *args, **kwargs):
        tools = Tool.objects.all()
        return TemplateResponse(request, 'tools_list.html', context={
            'tools': tools,
            'hero': {
                'text': 'tools'
            }
        })


class ToolView(View):
    def get(self, request, slug, *args, **kwargs) -> Union[TemplateResponse, HttpResponseRedirect]:
        try:
            tool_object = Tool.objects.get(slug=slug)
            types = [slugify(type_name) for type_name in tool_object.types.all().values_list('name', flat=True)]
            links = tool_object.download_links.all()
            main_type = slugify(tool_object.main_type)
            return TemplateResponse(request, 'tool.html', context={
                'tool': tool_object,
                'main_type': main_type,
                'types': types,
                'links': [{'device': link.device, 'link': link.link, 'icon': link.icon_name} for link in links],
                'hero': {
                    'text': tool_object.name
                }
            })
        except Tool.DoesNotExist:
            prev = request.GET.get('previous', '/')
            messages.add_message(request, messages.ERROR, 'Tool does not exist, sorry, try another.')
            return HttpResponseRedirect(prev)


tool_view = ToolView.as_view()
all_tools = AllTools.as_view()


class FundView(View):
    def get(self, request, slug, *args, **kwargs) -> Union[TemplateResponse, HttpResponseRedirect]:
        try:
            fund = Fund.objects.get(slug=slug)
            return TemplateResponse(request, 'fund.html', context={
                'fund': fund,
                'hero': {
                    'text': fund.name
                }
            })
        except Fund.DoesNotExist:
            prev = request.GET.get('previous', '/')
            messages.add_message(request, messages.ERROR, 'Fund does not exist, sorry, try another.')
            return HttpResponseRedirect(prev)


class AllFunds(View):
    def get(self, request, *args, **kwargs):
        funds = Fund.objects.all()
        return TemplateResponse(request, 'funds_list.html', context={
            'funds': funds,
            'hero': {
                'text': 'funds'
            }
        })


fund_view = FundView.as_view()
all_funds = AllFunds.as_view()


class PetitionView(View):
    def get(self, request, slug, *args, **kwargs) -> Union[TemplateResponse, HttpResponseRedirect]:
        try:
            petition = Petition.objects.get(slug=slug)
            return TemplateResponse(request, 'petition.html', context={
                'fund': petition,
                'hero': {
                    'text': petition.name
                }
            })
        except Fund.DoesNotExist:
            prev = request.GET.get('previous', '/')
            messages.add_message(request, messages.ERROR, 'Petition does not exist, sorry, try another.')
            return HttpResponseRedirect(prev)


class AllPetitions(View):
    def get(self, request, *args, **kwargs):
        petitions = Petition.objects.all()
        return TemplateResponse(request, 'petition_list.html', context={
            'petitions': petitions,
            'hero': {
                'text': 'Petitions'
            }
        })


petition_view = PetitionView.as_view()
all_petitions = AllPetitions.as_view()


class LegalView(View):
    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, 'legal.html')


legal_home = LegalView.as_view()


class EncryptText(View):
    def get(self, request, *args, **kwargs):
        return TemplateResponse(request, 'encryption.html')


encrypt_text = EncryptText.as_view()


class ListAuthorPosts(ListView):
    def get(self, request, *args, **kwargs):
        pages = BlogPage.objects.filter(author=request.data['author'])
        return TemplateResponse(request, 'posts.html', context={'pages': pages})


list_author_pages = ListAuthorPosts.as_view()


def generate_random_quote() -> list:
    quotes = [
        '"This and no other is the root from which a tyrant springs; when he first appears, he is a protector." - Plato',
        '"I want you to know that everything I did, I did for my country." - Pol Pot',
        '"Good intentions will always be pleaded for every assumption of authority... the Constitution was made to guard'
        ' against the dangers of good intentions." - Daniel Webster',
        '“This work was strictly voluntary, but any animal who absented himself from it would have his rations reduced by half.” ― George Orwell, Animal Farm ',
        '"If Tyranny and Oppression come to this land, it will be in the guise of fighting a foreign enemy." - James Madison',
        '"There is no crueler tyranny than that which is perpetuated under the shield of law and in the name of justice." - Baron de Montesquieu',
        '"The greatest tyrannies are always perpetuated in the name of the noblest causes." - Thomas Paine',
        '"I have sworn upon the altar of God Eternal, hostility against every form of tyranny over the mind of man." - Thomas Jefferson',
        '"The price of freedom is eternal vigilance." - Desmond Tutu'
    ]

    return quotes
