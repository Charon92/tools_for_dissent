from django import forms
from django.contrib import admin

from tools_for_dissent.models import Recommendation


class RecommendationForm(forms.ModelForm):

    class Meta:
        model = Recommendation
        fields = ['name', 'website', 'description', 'reason']
