"""tools_for_dissent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from . import views

urlpatterns = [
    path(os.getenv('SECRET_ADMIN_URL'), admin.site.urls),
    re_path(r'^cms/', include(wagtailadmin_urls)),
    re_path(r'^documents/', include(wagtaildocs_urls)),
    re_path(r'^articles/', include(wagtail_urls)),
    path('tinymce/', include('tinymce.urls')),
    path('api/', include('tools_for_dissent.api_urls')),
    path('', views.index, name='home'),
    path('legal/', views.legal_home, name='legal_home'),
    path('tools/', views.all_tools, name='tools'),
    path('tools/<slug:slug>/', views.tool_view, name='tool_page'),
    path('funds/', views.all_funds, name='funds'),
    path('fund/<slug:slug>', views.fund_view, name='fund_page'),
    path('petitions/', views.all_petitions, name='petitions'),
    path('petition/<slug:slug>', views.petition_view, name='petition_page'),
    path('password_generator/', views.password_generator, name='password_generator'),
    path('encryption/', views.encrypt_text, name='encrypt_page'),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.STATIC_URL,
        document_root=settings.STATIC_ROOT
    ) + static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
