# Generated by Django 3.0.6 on 2020-06-03 17:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tools_for_dissent', '0007_add_recommendation_model'),
    ]

    operations = [
        migrations.CreateModel(
            name='Fund',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=512)),
                ('cause', models.CharField(max_length=1024)),
                ('link', models.URLField()),
                ('description', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Petition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=512)),
                ('link', models.URLField()),
                ('description', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='ProtestingTipArticle',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('link', models.URLField()),
            ],
        ),
        migrations.AlterField(
            model_name='type',
            name='tools',
            field=models.ManyToManyField(blank=True, related_name='types', to='tools_for_dissent.Tool'),
        ),
        migrations.CreateModel(
            name='ProtestingTip',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tip', models.TextField()),
                ('icon', models.ImageField(upload_to='')),
                ('link_to_related_article', models.ManyToManyField(to='tools_for_dissent.ProtestingTipArticle')),
            ],
            options={
                'verbose_name_plural': 'Protesting Tips',
            },
        ),
    ]
